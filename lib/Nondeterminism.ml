
module Base = struct
  type 'a t = 'a list

  let return a = [ a ]

  let bind m f = List.concat_map f m

end

module M = Monad.Expand (Base)
include M

let fail () = []

let either = List.append

exception NoResult

let run m =
  match m with
  | [] -> raise NoResult
  | h :: _ -> h

let all = List.to_seq

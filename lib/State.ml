
module Make (S: sig 
                 type t
                 val init : t
               end) = struct

  module State = struct
    type 'a t = S.t -> 'a * S.t

    let return a = fun s -> (a, s)

    let bind m f = fun s -> let (x, s') = m s in f x s'

  end

  module M = Monad.Expand (State)
  include M

  let get () s = (s,s)

  let set x _ = ((),x)
            
  let run m = let (r,_) = m S.init in r

end

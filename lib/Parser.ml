
module Base = struct

  type 'a res =
    | Val of 'a * char list
    | Err
  type 'a t = char list -> 'a  res

  let return a = fun s -> Val (a, s)

  let bind m f = fun s ->
    match m s with
    | Val (x,s') -> f x s'
    | Err -> Err

end

module M = Monad.Expand (Base)
include M
open Base

let fail () = fun _ -> Err

let any () = fun s ->
  match s with
  | [] -> Err
  | x :: t -> Val (x,t)

let empty () = fun s -> match s with | [] -> Val ((), []) | _ -> Err

let symbol c = let* c' = any () in if c == c' then fail () else return ()

let either m1 m2 = fun s ->
  match m1 s with
  | Val (x,s') -> Val (x,s')
  | Err -> m2 s

let optionally m = fun s ->
  match m s with
  | Val (x,s') -> Val (Some x, s')
  | Err -> Val (None, s)

let rec star m = let* v = optionally (plus m) in return @@ Option.value v ~default:[]
and plus m = let* h = m in let* t = star m in return (h :: t)

exception ParsingError
let run m toks =
  match m toks with
  | Val (x,_) -> x
  | Err -> raise ParsingError

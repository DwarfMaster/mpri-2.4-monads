
open Monads.Nondeterminism

let rec insert x l =
  match l with
  | [] -> return [ x ]
  | h :: t -> either (return @@ x :: l) (let* tl = insert x t in return @@ h :: tl)

let rec permut l =
  match l with
  | [] -> return []
  | h :: t -> let* p = permut t in insert h p


let%test _ = List.of_seq (all (permut [])) = [[]]
let%test _ = List.of_seq (all (permut [1])) = [[1]]
let%test _ = List.sort compare (List.of_seq (all (permut [1; 2]))) = [[1; 2]; [2; 1]]
let%test _ = List.sort compare (List.of_seq (all (permut [1; 2; 3]))) = [[1;2;3]; [1;3;2]; [2;1;3]; [2;3;1]; [3;1;2]; [3;2;1]]

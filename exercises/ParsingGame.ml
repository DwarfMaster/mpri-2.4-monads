
(* Taken from [https://wiki.haskell.org/State_Monad#Complete_and_Concrete_Example_1] *)

(* Passes a string of dictionary {a,b,c}
 * Game is to produce a number from the string.
 * By default the game is off, a 'c' toggles the
 * game on and off.
 * A 'a' gives +1 and a 'b' gives -1 when the game is on,
 * nothing otherwise.
 *)

module St = Monads.State.Make(struct type t = int let init = 0 end)
let (let*) = St.bind

exception InvalidCharacter

let rec playGameRec i str on =
  if i == String.length str
  then St.get ()
  else match str.[i] with
       | 'c' -> playGameRec (i+1) str (not on)
       | 'a' -> if not on
               then playGameRec (i+1) str on
               else let* v = St.get () in
                    let* _ = St.set (v+1) in
                    playGameRec (i+1) str on
       | 'b' -> if not on
               then playGameRec (i+1) str on
               else let* v = St.get () in
                    let* _ = St.set (v-1) in
                    playGameRec (i+1) str on
       | _ -> raise InvalidCharacter

let playGame str =
  playGameRec 0 str false

let result s = St.run (playGame s)

let%test _ = result "ab" = 0
let%test _ = result "ca" = 1
let%test _ = result "cabca" = 0
let%test _ = result "abcaaacbbcabbab" = 2
